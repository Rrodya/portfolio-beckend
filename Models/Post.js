import mongoose from "mongoose";

const Post = new mongoose.Schema({
    img: {type: String, required: true},
    title: {type: String, required: true},
    count: {type: String, required: true},
})

export default mongoose.model('Post', Post);
