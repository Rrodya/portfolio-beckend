import mongoose from "mongoose";

const Recipes = new mongoose.Schema({
    name: {type: String, required: true},
    image: {type: String, required: false},
    description: {type: String, required: true},
    category: [{name: String, title:     String}],
    compound: [{title: String, name: String}],
    isPopular: {type: Boolean, required: false, default: false},
    steps: [{step: String,  require: false}]
})

export default mongoose.model('Recipes', Recipes);
