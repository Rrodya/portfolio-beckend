import mongoose from "mongoose";

const Vacancies = new mongoose.Schema({
    path: {type: String, required: true},
    link: {type: String, required: true},
})

export default mongoose.model('Vacancies', Vacancies);
