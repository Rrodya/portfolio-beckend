import mongoose from "mongoose";

const ForumArticles = new mongoose.Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    messages: [{image: String, name: String, countArticles: String, message: String}],
    category: [{name: String, title: String}],
    isPopular: {type: Boolean, required: false, default: false}
})

export default mongoose.model('ForumArticles', ForumArticles);
