import Recipes from "../Models/Recipes.js";

class RecipesService {
    async create(recipe) {
        const createdRecipe = await Recipes.create(recipe);
        return createdRecipe;
    }

    async getAll() {
        const recipes = await Recipes.find();
        return recipes;
    }

    async getOne(id) {
        if(!id) {
            throw new Error('ID не был указан')
        }

        const recipe = await Recipes.findById(id);
        return post
    }
}

export default new RecipesService();
