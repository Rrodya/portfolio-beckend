import ForumArcticles from "../Models/ForumArcticles.js";

class RecipesService {
    async create(recipe) {
        const createForumItem = await ForumArcticles.create(recipe);
        return createForumItem;
    }

    async getAll() {
        const forumItems = await ForumArcticles.find();
        return forumItems;
    }

    async getOne(id) {
        if(!id) {
            throw new Error('ID не был указан')
        }

        const forumItem = await ForumArcticles.findById(id);
        return forumItem
    }
}

export default new RecipesService();
