import ForumService from "../Services/ForumService.js";

class CategoryController {
    async create(req, res){
        try {
            const forumItem = await ForumService.create(req.body);
            return res.json(forumItem);
        } catch (e) {
            res.status(500).json(e);
        }
    }

    async getAll(req, res){
        try {
            const forumItems = await ForumService.getAll();
            return res.json(forumItems);
        } catch (e) {
            res.status(500).json(e);
        }
    }

    async getOne(req, res){
        try {
            const forumItem = await ForumService.getOne(req.body._id);
            return res.json(forumItem);
        } catch (e) {
            res.status(500).json(e);
        }
    }
}

export default new CategoryController();
